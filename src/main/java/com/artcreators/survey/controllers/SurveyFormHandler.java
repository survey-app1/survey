package com.artcreators.survey.controllers;

import com.artcreators.survey.entities.Submission;
import com.artcreators.survey.entities.documents.SubmittedSurveyDoc;
import com.artcreators.survey.exceptions.AllAttemptsExhaustedException;
import com.artcreators.survey.services.SubmissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class SurveyFormHandler {

    private SubmissionService submissionService;

    @Autowired
    public SurveyFormHandler(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }

    @PostMapping("/survey")
    public ResponseEntity<SubmittedSurveyDoc> handleForm(@RequestBody Submission submission) {
        log.info("Submitted survey received {}", submission);
        SubmittedSurveyDoc savedDoc = submissionService.save(submission);
        String filePath = submissionService.generatePdf(savedDoc);
//        submissionService.sendMailToOdmin("Report for user " + submission.getPersonnelNumber(), "Results are attached", filePath);
        return ResponseEntity.ok(savedDoc);
    }

    @GetMapping("/submissions")
    public List<SubmittedSurveyDoc> listAll() {
        return submissionService.getAll();
    }

    @DeleteMapping("/submissions")
    public void clearAll() {
        submissionService.deleteAll();
    }

    @DeleteMapping("/submissions/{id}")
    public void clearOne(@PathVariable("id") String id) {
        submissionService.deleteById(id);
    }

    @GetMapping("/submission/{id}")
    public ResponseEntity<Resource> getFile(@PathVariable("id") String id) {
        Resource pdfFileResource = submissionService.getSubmissionAsResource(id);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + pdfFileResource.getFilename() + "\"")
                .body(pdfFileResource);
    }


    @ExceptionHandler(AllAttemptsExhaustedException.class)
    public ResponseEntity<?> handleNoAttemptsLeft() {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}


