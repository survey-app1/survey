package com.artcreators.survey.controllers;

import com.artcreators.survey.entities.Answer;
import com.artcreators.survey.entities.Question;
import com.artcreators.survey.entities.documents.QuestionDoc;
import com.artcreators.survey.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuestionController {

    private QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/questions")
    public List<QuestionDoc> getQuestions() {
        return questionService.getQuestions();
    }

    @PostMapping("/questions")
    public ResponseEntity<?> addQuestions(@RequestBody List<Question> questions) {

        boolean hasCorrectAnswer = questions.stream().allMatch(question -> question.getAvailableAnswers()
                .stream().map(Answer::getText).anyMatch(answer -> answer.equals(question.getCorrectAnswerText())));
        if (! hasCorrectAnswer) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Correct answer text should be one of the options");
        }

        questionService.saveQuestions(questions);
        return ResponseEntity.ok(questionService.getQuestions());
    }

    @DeleteMapping("/questions")
    public ResponseEntity<List<QuestionDoc>> clearQuestions() {
        questionService.clearQuestions();
        return ResponseEntity.ok(questionService.getQuestions());
    }

}
