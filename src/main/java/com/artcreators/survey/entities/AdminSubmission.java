package com.artcreators.survey.entities;

import lombok.Data;

@Data
public class AdminSubmission {

    private String submissionId;
    private String personnelNumber;
    private int totalScore;
}
