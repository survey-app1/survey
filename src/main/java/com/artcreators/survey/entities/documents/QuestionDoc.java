package com.artcreators.survey.entities.documents;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("question")
public class QuestionDoc {
    @Id
    private String id;
    private String language;
    private String text;
    private String title;
    private List<AnswerDoc> availableAnswers;
    private String correctAnswerId;
}