package com.artcreators.survey.entities.documents;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("answer")
public class AnswerDoc {

    @Id
    private String id;
    private String text;

    public AnswerDoc(String text) {
        this.text = text;
    }
}