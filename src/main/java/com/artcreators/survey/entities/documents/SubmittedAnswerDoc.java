package com.artcreators.survey.entities.documents;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubmittedAnswerDoc {

    private String questionId;
    private String answerId;
    private boolean isCorrect;
}
