package com.artcreators.survey.entities.documents;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("submission")
@Data
public class SubmittedSurveyDoc {

    @Id
    private String id;

    private String personnelNumber;
    private String locale;

    private List<SubmittedAnswerDoc> answers;

    @PersistenceConstructor
    public SubmittedSurveyDoc() {
        answers = new ArrayList<>();
    }
}
