package com.artcreators.survey.entities;

import com.artcreators.survey.entities.documents.AnswerDoc;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class Question {

    private String text;
    private String language;
    private String title;
    private List<Answer> availableAnswers;
    private String correctAnswerText;
}