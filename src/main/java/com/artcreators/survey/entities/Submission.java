package com.artcreators.survey.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class Submission {

    private String personnelNumber;
    private String locale;
    private List<SubmittedAnswer> answers;
}
