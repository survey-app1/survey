package com.artcreators.survey.entities;

import lombok.Data;

@Data
public class SubmittedAnswer {

    private String questionId;
    private String answerId;
}
