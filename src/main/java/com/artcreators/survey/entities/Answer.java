package com.artcreators.survey.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Answer {

    private String text;
}