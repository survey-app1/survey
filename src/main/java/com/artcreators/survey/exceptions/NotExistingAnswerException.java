package com.artcreators.survey.exceptions;

public class NotExistingAnswerException extends RuntimeException {
    public NotExistingAnswerException(String message) {
        super(message);
    }
}
