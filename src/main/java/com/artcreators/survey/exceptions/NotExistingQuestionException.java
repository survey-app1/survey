package com.artcreators.survey.exceptions;

public class NotExistingQuestionException extends RuntimeException {
    public NotExistingQuestionException(String message) {
        super(message);
    }
}
