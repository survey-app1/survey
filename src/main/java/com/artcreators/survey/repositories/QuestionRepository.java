package com.artcreators.survey.repositories;

import com.artcreators.survey.entities.documents.QuestionDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QuestionRepository extends MongoRepository<QuestionDoc, String> {
}
