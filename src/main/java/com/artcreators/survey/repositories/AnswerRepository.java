package com.artcreators.survey.repositories;

import com.artcreators.survey.entities.documents.AnswerDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AnswerRepository extends MongoRepository<AnswerDoc, String> {
}
