package com.artcreators.survey.repositories;

import com.artcreators.survey.entities.documents.SubmittedSurveyDoc;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SubmissionRepository extends MongoRepository<SubmittedSurveyDoc, String> {

    List<SubmittedSurveyDoc> findAllByPersonnelNumber(String personnelNumber);
}
