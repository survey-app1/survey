package com.artcreators.survey.services;

import com.artcreators.survey.entities.Submission;
import com.artcreators.survey.entities.documents.QuestionDoc;
import com.artcreators.survey.entities.documents.SubmittedAnswerDoc;
import com.artcreators.survey.entities.documents.SubmittedSurveyDoc;
import com.artcreators.survey.exceptions.NotExistingQuestionException;
import com.artcreators.survey.repositories.QuestionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@Slf4j
public class SurveyMapperService {

    private QuestionRepository questionRepository;

    @Autowired
    public SurveyMapperService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public SubmittedSurveyDoc mapToDocument(Submission submission){
        SubmittedSurveyDoc surveyDoc = new SubmittedSurveyDoc();
        surveyDoc.setLocale(submission.getLocale());
        surveyDoc.setPersonnelNumber(submission.getPersonnelNumber());
        surveyDoc.setAnswers(new ArrayList<>());
        submission.getAnswers().forEach(submittedAnswer -> {
            QuestionDoc questionDoc = questionRepository.findById(submittedAnswer.getQuestionId())
                    .orElseThrow(() -> new NotExistingQuestionException("No question with id" + submittedAnswer.getQuestionId()));

            surveyDoc.getAnswers().add(new SubmittedAnswerDoc(
                    submittedAnswer.getQuestionId(),
                    submittedAnswer.getAnswerId(),
                    questionDoc.getCorrectAnswerId().equals(submittedAnswer.getAnswerId())));
        });
        log.info("Mapped submission: {}", surveyDoc);
        return surveyDoc;
    }
}
