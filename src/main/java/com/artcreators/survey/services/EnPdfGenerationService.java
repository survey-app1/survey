package com.artcreators.survey.services;

import com.artcreators.survey.entities.documents.AnswerDoc;
import com.artcreators.survey.entities.documents.QuestionDoc;
import com.artcreators.survey.entities.documents.SubmittedAnswerDoc;
import com.artcreators.survey.entities.documents.SubmittedSurveyDoc;
import com.artcreators.survey.exceptions.NotExistingQuestionException;
import com.artcreators.survey.repositories.QuestionRepository;
import lombok.SneakyThrows;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class EnPdfGenerationService {

    private QuestionRepository questionService;

    @Autowired
    public EnPdfGenerationService(QuestionRepository questionService) {
        this.questionService = questionService;
    }

    @SneakyThrows(IOException.class)
    public String generatePdf(SubmittedSurveyDoc submittedSurveyDoc) {
        PDDocument pdDocument = new PDDocument();

        PDDocumentInformation pdDocumentInformation = new PDDocumentInformation();
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date());
        pdDocumentInformation.setCreationDate(instance);
        pdDocumentInformation.setTitle("Safety Test Results");

        PDType0Font font = PDType0Font.load(pdDocument, new File("./resources/TimesNewRoman.ttf"));
        PDType0Font boldFont = PDType0Font.load(pdDocument, new File("./resources/TimesNewRomanBold.ttf"));


        PDPage pdPage = new PDPage();
        float height = pdPage.getMediaBox().getHeight();
        float width = pdPage.getMediaBox().getWidth();
        PDPageContentStream pdContentStream = new PDPageContentStream(pdDocument, pdPage);
        pdContentStream.beginText();
        pdContentStream.setFont(boldFont, 14);
        pdContentStream.setLeading(14.5f);
        pdContentStream.newLineAtOffset(width / 4, height - 30f);
        pdContentStream.showText("Safety Test Results");
        pdContentStream.endText();
        pdContentStream.beginText();
        pdContentStream.newLineAtOffset(width / 8, height - 30f - 20f);
        pdContentStream.setFont(font, 12);
        pdContentStream.showText("Employee Personnel Number" + submittedSurveyDoc.getPersonnelNumber());
        pdContentStream.newLine();
        pdContentStream.showText("Date " + DateTimeFormatter.ofPattern("dd.MM.yyyy").format(LocalDate.now(Clock.systemUTC())));
        pdContentStream.endText();
        pdContentStream.beginText();
        pdContentStream.setLeading(14.5f);
        pdContentStream.newLineAtOffset(width / 8, height - 90f);
        List<SubmittedAnswerDoc> answers = submittedSurveyDoc.getAnswers();

        for (int i = 0; i < answers.size(); i++) {
            SubmittedAnswerDoc answer = answers.get(i);
            QuestionDoc questionDoc = questionService.findById(answer.getQuestionId())
                    .orElseThrow(() -> new NotExistingQuestionException("Can't find question while generating pdf"));
            pdContentStream.setFont(boldFont, 12);
            showStringByChunks(pdContentStream, String.format("%d. %s", i + 1, questionDoc.getText()), 70);
            pdContentStream.setFont(font, 12);
            List<AnswerDoc> availableAnswers = questionDoc.getAvailableAnswers();
            for (int answrCounter = 0; answrCounter < availableAnswers.size(); answrCounter++) {
                AnswerDoc availableAnswer = availableAnswers.get(answrCounter);
                String text = String.format("%d) %s", answrCounter + 1,
                        availableAnswer.getText()) + addCaptionForUserAnswer(answer, availableAnswer);
                showStringByChunks(pdContentStream, text, 90);

            }
        }
        pdContentStream.endText();
//        addCaption(height, width, pdContentStream, answers);

        pdDocument.addPage(pdPage);

        pdDocument.setDocumentInformation(pdDocumentInformation);
        pdContentStream.close();
        String pathname = "./files/" + submittedSurveyDoc.getId() + ".pdf";
        pdDocument.save(new File(pathname));
        pdDocument.close();
        return pathname;
    }

    private void addCaption(float height, float width, PDPageContentStream pdContentStream, List<SubmittedAnswerDoc> answers) throws IOException {
        pdContentStream.beginText();
        pdContentStream.newLineAtOffset(width / 8, height - (90f + getQuestionsHeight(answers)));
        pdContentStream.showText("Руководитель службы безопасности:");
        pdContentStream.endText();
        pdContentStream.beginText();
        pdContentStream.newLineAtOffset(width / 2, height - (90f + (getQuestionsHeight(answers))));
        pdContentStream.showText("Сотрудник:");
        pdContentStream.endText();
        pdContentStream.beginText();
        pdContentStream.newLineAtOffset(width / 8, height - (90f + (getQuestionsHeight(answers)) + 20f));
        pdContentStream.showText("________/______________________/");
        pdContentStream.endText();
        pdContentStream.beginText();
        pdContentStream.newLineAtOffset(width / 2, height - (90f + (getQuestionsHeight(answers)) + 20f));
        pdContentStream.showText("________/______________________/");
        pdContentStream.endText();
        pdContentStream.beginText();
        pdContentStream.newLineAtOffset(width / 8, height - (90f + (getQuestionsHeight(answers)) + 20f + 22f));
        pdContentStream.showText("Подпись /                ФИО");
        pdContentStream.endText();
        pdContentStream.beginText();
        pdContentStream.newLineAtOffset(width / 2, height - (90f + (getQuestionsHeight(answers)) + 20f + 22f));
        pdContentStream.showText("Подпись /                ФИО");
        pdContentStream.endText();
    }

    private float getQuestionsHeight(List<SubmittedAnswerDoc> answers) {
        return answers.size() * 150f;
    }

    private void showStringByChunks(PDPageContentStream pdContentStream, String text, int lineWidth) throws IOException {
        //TODO find the closest space back in order not to break words
        //TODO BUG begin index can ve > endndex
        int beginIndex = 0;
        do {
            if (text.length() - beginIndex > lineWidth) {
                int end = Math.min(beginIndex + lineWidth, text.length());
                int endIndex = text.lastIndexOf(" ", end);
                if (endIndex < beginIndex) {
                    break;
                }
                String substring = text.substring(beginIndex, endIndex != -1 ? endIndex : end);

                pdContentStream.showText(substring);
                pdContentStream.newLine();
            }
            else {
                String substring = text.substring(beginIndex);
                pdContentStream.showText(substring);
                pdContentStream.newLine();
            }
            beginIndex += lineWidth;
        }
        while (beginIndex  < text.length());
    }

    private String addCaptionForUserAnswer(SubmittedAnswerDoc answer, AnswerDoc availableAnswer) {
        return availableAnswer.getId().equals(answer.getAnswerId()) ? (answer.isCorrect() ? " (True) " : " (False) ") : "";
    }
}