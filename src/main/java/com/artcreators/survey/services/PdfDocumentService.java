package com.artcreators.survey.services;

import com.artcreators.survey.entities.documents.SubmittedSurveyDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PdfDocumentService {

    private EnPdfGenerationService enPdfGenerationService;
    private RuPdfGenerationService ruPdfGenerationService;

    @Autowired
    public PdfDocumentService(EnPdfGenerationService enPdfGenerationService, RuPdfGenerationService ruPdfGenerationService) {
        this.enPdfGenerationService = enPdfGenerationService;
        this.ruPdfGenerationService = ruPdfGenerationService;
    }

    public String generatePdf(SubmittedSurveyDoc submittedSurveyDoc) {
        switch (submittedSurveyDoc.getLocale()) {
            case "en":
                return enPdfGenerationService.generatePdf(submittedSurveyDoc);
            case "ru":
                return ruPdfGenerationService.generatePdf(submittedSurveyDoc);
            default:
                return enPdfGenerationService.generatePdf(submittedSurveyDoc);
        }
    }

}