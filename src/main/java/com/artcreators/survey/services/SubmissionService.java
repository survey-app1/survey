package com.artcreators.survey.services;

import com.artcreators.survey.entities.Submission;
import com.artcreators.survey.entities.documents.SubmittedSurveyDoc;
import com.artcreators.survey.exceptions.AllAttemptsExhaustedException;
import com.artcreators.survey.repositories.SubmissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubmissionService {

    private SubmissionRepository submissionRepository;
    private PdfDocumentService documentService;
    private MailService mailService;
    private SurveyMapperService surveyMapper;


    @Autowired
    public SubmissionService(SubmissionRepository submissionRepository,
                             PdfDocumentService documentService,
                             MailService mailService,
                             SurveyMapperService surveyMapper) {
        this.submissionRepository = submissionRepository;
        this.documentService = documentService;
        this.mailService = mailService;
        this.surveyMapper = surveyMapper;
    }

    public SubmittedSurveyDoc save(Submission submission) {
        List<SubmittedSurveyDoc> allByPersonnelNumber = submissionRepository.findAllByPersonnelNumber(submission.getPersonnelNumber());
        if (allByPersonnelNumber.size() > 2) {
            throw new AllAttemptsExhaustedException();
        }

        return submissionRepository.save(surveyMapper.mapToDocument(submission));
    }

    public void deleteAll() {
        submissionRepository.deleteAll();
    }

    public List<SubmittedSurveyDoc> getAll() {
        return submissionRepository.findAll();
    }

    public void deleteById(String id) {
        submissionRepository.deleteById(id);
    }

    public String generatePdf(SubmittedSurveyDoc doc) {
        return documentService.generatePdf(doc);
    }

    public void sendMailToOdmin(String subject, String text, String path) {
        mailService.sendMessageWithAttachment(subject, text, path);
    }

    public Resource getSubmissionAsResource(String submissionId) {
        return new FileSystemResource("files/"+submissionId+".pdf");
    }
}
