package com.artcreators.survey.services;

import com.artcreators.survey.entities.Answer;
import com.artcreators.survey.entities.Question;
import com.artcreators.survey.entities.documents.AnswerDoc;
import com.artcreators.survey.entities.documents.QuestionDoc;
import com.artcreators.survey.repositories.AnswerRepository;
import com.artcreators.survey.repositories.QuestionRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionService {

    private QuestionRepository questionRepository;
    private AnswerRepository answerRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository, AnswerRepository answerRepository) {
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
    }

    public List<QuestionDoc> getQuestions() {
        List<QuestionDoc> questionDocs = questionRepository.findAll();
        //TODO UNCOMMENT
//        questionDocs.forEach(questionDoc -> questionDoc.setCorrectAnswerId(null));
        return questionDocs;
    }

    public void saveQuestions(List<Question> questions) {
        List<QuestionDoc> questionDocs = questions.stream().map(question -> {
            val questionDoc = new QuestionDoc();
            questionDoc.setText(question.getText());
            questionDoc.setTitle(question.getTitle());
            questionDoc.setLanguage(question.getLanguage());
            questionDoc.setAvailableAnswers(new ArrayList<>());
            question.getAvailableAnswers().stream().map(Answer::getText).forEach(option -> {
                AnswerDoc savedAnswerDoc = answerRepository.save(new AnswerDoc(option));
                questionDoc.getAvailableAnswers().add(savedAnswerDoc);
                if (savedAnswerDoc.getText().equals(question.getCorrectAnswerText())) {
                    questionDoc.setCorrectAnswerId(savedAnswerDoc.getId());
                }
            });
            return questionDoc;
        }).collect(Collectors.toList());
        questionRepository.saveAll(questionDocs);
    }

    public void clearQuestions() {
        questionRepository.deleteAll();
    }
}
