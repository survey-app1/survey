pwd
service survey stop
git pull
./gradlew build
rm -rf /var/survey/survey.jar
cp ./build/libs/survey.jar /var/survey/
rm -rf /var/survey/resources/*
cp -r ./build/resources/* /var/survey/resources
service survey start